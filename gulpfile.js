'use strict';
var gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  	 tsc = require('gulp-tsc'),
	 del = require('del');

gulp.task('transpile', function() {
	return gulp.src('src/**/*.ts')
		.pipe(tsc({removeComments: true}))
		.pipe(rename('switchamajig.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('minify', ['transpile'], function() {
	return gulp.src('dist/switchamajig.js')
		.pipe(uglify({mangle: false}))
		.pipe(rename('switchamajig.min.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
	return del('dist');
});

gulp.task('watch', ['minify'], function() {
	gulp.watch('src/**/*.ts', ['minify']);
});

gulp.task('default', ['minify']);