(function ($, kula) {
    var formControl = kula.getStyles('input.form-control');
    var offset = formControl.borderBottomWidth === '0px' ? 4 : 0;
    var SwitchamajigOptions = (function () {
        function SwitchamajigOptions(opts) {
            this.borderRadius = opts.borderRadius || window['SwitchamajigDefaults'].borderRadius;
            this.width = opts.width || window['SwitchamajigDefaults'].width;
            this.margin = opts.margin || window['SwitchamajigDefaults'].margin;
            this.onText = opts.onText || window['SwitchamajigDefaults'].onText;
            this.offText = opts.offText || window['SwitchamajigDefaults'].offText;
            this.onColor = opts.onColor || window['SwitchamajigDefaults'].onColor;
            this.onTextColor = opts.onTextColor || window['SwitchamajigDefaults'].onTextColor;
            this.offColor = opts.offColor || window['SwitchamajigDefaults'].offColor;
            this.offTextColor = opts.offTextColor || window['SwitchamajigDefaults'].offTextColor;
            this.transform = opts.transform || window['SwitchamajigDefaults'].transform;
            this.toggleSpeed = opts.toggleSpeed || window['SwitchamajigDefaults'].toggleSpeed;
            this.toggleTimingFunction = opts.toggleTimingFunction || window['SwitchamajigDefaults'].toggleTimingFunction;
            this.afterInit = opts.afterInit || window['SwitchamajigDefaults'].afterInit;
            this.beforeInit = opts.beforeInit || window['SwitchamajigDefaults'].beforeInit;
            this.afterChange = opts.afterChange || window['SwitchamajigDefaults'].afterChange;
            this.beforeChange = opts.beforeChange || window['SwitchamajigDefaults'].beforeChange;
        }
        return SwitchamajigOptions;
    }());
    window['SwitchamajigDefaults'] = {
        borderRadius: formControl.borderTopLeftRadius,
        width: ((Number(formControl.height.split('px')[0]) * 0.5) * 2.5) + "px",
        margin: '0',
        onText: '&ZeroWidthSpace;',
        offText: '&ZeroWidthSpace;',
        onColor: kula.getStyle('.panel.panel-primary .panel-heading', 'background-color'),
        offColor: kula.darken($('body').css('background-color'), 5),
        transform: '',
        toggleSpeed: '0.2s',
        toggleTimingFunction: 'ease',
        afterInit: function () { },
        beforeInit: function () { },
        afterChange: function () { },
        beforeChange: function () { }
    };
    window['SwitchamajigDefaults'].onTextColor = kula.lightLevel(window['SwitchamajigDefaults'].onColor) < 75 ? '#fff' : '#000';
    window['SwitchamajigDefaults'].offTextColor = kula.lightLevel(window['SwitchamajigDefaults'].offColor) < 75 ? '#fff' : '#000';
    var Switchamajig = (function () {
        function Switchamajig(inputElement, opts) {
            this.inputElement = inputElement;
            this.id = "smj-" + kula.guid();
            this.options = new SwitchamajigOptions(opts);
        }
        Switchamajig.prototype.on = function () {
            this.inputElement.prop('checked', true);
            this.linkState();
        };
        Switchamajig.prototype.off = function () {
            this.inputElement.prop('checked', false);
            this.linkState();
        };
        Switchamajig.prototype.toggle = function () {
            this.inputElement.prop('checked', !this.inputElement.is(':checked'));
            this.linkState();
        };
        Switchamajig.prototype.linkVisualState = function () {
            if (this.inputElement.is(':checked')) {
                $("#" + this.id).css('background-color', this.options.onColor);
                $("#" + this.id + " span:first-child").css('margin-left', '0');
            }
            else {
                $("#" + this.id).css('background-color', this.options.offColor);
                $("#" + this.id + " span:first-child").css('margin-left', "-" + ((Number($("#" + this.id).css('width').split('px')[0]) * 0.65) - (offset + (Number(formControl.borderBottomWidth.split('px')[0]) * 2))) + "px");
            }
        };
        Switchamajig.prototype.linkState = function () {
            this.linkVisualState();
            var time = parseFloat(this.options.toggleSpeed) * 1000;
            var input = this.inputElement;
            setTimeout(function () {
                input.change();
            }, time);
        };
        return Switchamajig;
    }());
    function buildElements(smj) {
        smj.inputElement.hide();
        smj.inputElement.after("<div id=\"" + smj.id + "\"><span>" + smj.options.onText + "</span><div></div><span>" + smj.options.offText + "</span></div>");
    }
    function setStyles(smj) {
        var boxShadow = kula.getStyle('.panel.panel-default', 'box-shadow');
        $("#" + smj.id)
            .css('width', smj.options.width)
            .css('height', ((Number(formControl.height.split('px')[0]) * 0.5) + (offset / 2) + (parseInt(formControl.borderBottomWidth) * 2)) + "px")
            .css('overflow', 'hidden')
            .css('padding', kula.currentBrowser === 'Chrome' || kula.currentBrowser === 'Opera' ? '2px 3px' : '1px 3px')
            .css('transition', "all " + smj.options.toggleSpeed + " " + smj.options.toggleTimingFunction)
            .css('background-color', smj.options.offColor)
            .css('border-color', formControl.borderBottomColor)
            .css('border-width', formControl.borderBottomWidth)
            .css('border-radius', smj.options.borderRadius)
            .css('border-style', formControl.borderBottomStyle)
            .css('transform', smj.options.transform)
            .css('margin', smj.options.margin)
            .css('user-select', 'none')
            .css('cursor', 'pointer')
            .css('white-space', 'nowrap')
            .css('box-shadow', formControl.boxShadow);
        $(".text-right #" + smj.id)
            .css('float', 'right');
        $(".text-center #" + smj.id)
            .css('margin', '0 auto');
        $(".text-left #" + smj.id)
            .css('float', 'left');
        $("#" + smj.id + " div")
            .css('margin-right', '0')
            .css('display', 'inline-block')
            .css('width', ((parseFloat($("#" + smj.id).css('width')) * 0.35) - 2) + "px")
            .css('padding', '0')
            .css('height', ((parseFloat(formControl.height) * 0.5) - 4) + "px")
            .css('transition', "all " + smj.options.toggleSpeed + " " + smj.options.toggleTimingFunction)
            .css('border-color', formControl.borderBottomColor)
            .css('border-width', formControl.borderBottomWidth)
            .css('border-radius', smj.options.borderRadius)
            .css('border-style', formControl.borderBottomStyle)
            .css('background-color', $('body').css('background-color'))
            .css('user-select', 'none')
            .css('cursor', 'pointer')
            .css('box-shadow', boxShadow);
        if (kula.browser() === 'Safari') {
            $("#" + smj.id + " div").css('width', '35%');
        }
        $("#" + smj.id + " span")
            .css('vertical-align', 'top')
            .css('text-align', 'center')
            .css('width', '65%')
            .css('font-size', '0.8em')
            .css('display', 'inline-block');
        $("#" + smj.id + " span:first-child")
            .css('margin-left', '-66%')
            .css('transition', "all " + smj.options.toggleSpeed + " " + smj.options.toggleTimingFunction)
            .css('color', smj.options.onTextColor);
        $("#" + smj.id + " span:last-child")
            .css('color', smj.options.offTextColor);
        smj.linkVisualState();
    }
    function setListeners(smj) {
        $("#" + smj.id).mousedown(function () {
            smj.options.beforeChange();
            smj.toggle();
            smj.options.afterChange();
        });
    }
    function start(opts) {
        if (opts === void 0) { opts = {}; }
        window['Switchamajigs'] === undefined ? window['Switchamajigs'] = [] : null;
        for (var i = 0; i < window['Switchamajigs'].length; i++) {
            if ($(this).attr('id') === window['Switchamajigs'][i].inputElement.attr('id')) {
                return window['Switchamajigs'][i];
            }
        }
        var smj = new Switchamajig(this, opts);
        smj.options.beforeInit();
        buildElements(smj);
        setStyles(smj);
        setListeners(smj);
        window['Switchamajigs'].push(smj);
        smj.options.afterInit();
        return window['Switchamajigs'][window['Switchamajigs'].length - 1];
    }
    $.fn.extend({
        switchamajig: start
    });
})(window['jQuery'], window['kula']);
