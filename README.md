## Synopsis

A jQuery/Bootstrap3 plugin for turning checkboxes into toggle switches.

## Code Example

```javascript
$('#element').switchamajig();
```

## Motivation

This project was created out of a need for a visually reliable and easy to use bootstrap switch plugin after other bootstrap switch plugins have turned out to be less than acceptable to use.

## Installation

```
npm install --save switchamajig
```

## API Reference

If this plugin happens to gain enough popularity, it may be possible in the future to host a reference site. At this point, however, a code reference can be found in the index.html file within the npm package, which will be at the file path of ./node_modules/switchamajig/index.html .

## Tests

No tests implemented at this point in development. However, the aforementioned api reference acts as a simple set of tests for the time being.

## Contributors

Want to contribute? Clone the repo and follow through the CONTRIBUTORS file for details on how you can help the project out.

## License

MIT